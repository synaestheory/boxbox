import Head from "next/head";
import { useState, useEffect, useRef } from "react";
import Game from "../src/Game";
const Home = () => {
  const [game, setGame] = useState<Game>();
  const [canvasSize, setCanvasSize] = useState<{ w: number, h: number }>({ w: 1920, h: 1080 });
  const canvas = useRef(null);
  useEffect(() => setGame(new Game(window.innerWidth, window.innerHeight)), []);
  useEffect(() => {
    if (game && canvas.current) {
      game.canvas = canvas.current;
    }
  }, [game, canvas.current]);
  useEffect(() => {
    window.addEventListener("orientationchange", () => {
      if (game) game.orientationChange();
    });
  })
  useEffect(() => {
    const resizeHandler = () => {
      if (game) {
        game.resize(window.innerWidth, window.innerHeight);
      }
    };
    window.addEventListener('resize', resizeHandler);
    return () => window.removeEventListener('resize', resizeHandler);
  });
  return (
    <div className="container">
      <Head>
        <title>BoxBox</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="manifest" href="/manifest.json" />
      </Head>

      <main>
        <canvas width={canvasSize.w} height={canvasSize.h} ref={canvas}></canvas>
      </main>
      <style jsx>
        {`
        canvas {
          margin: auto;
          border: 0px;
          max-width: 100vw;
          max-height: 100vh;    
        }

        main {
          display: flex;
          justify-items: center;
          align-items: center;
          flex-direction: column;
          overflow: none;
        }
        
      `}
      </style>
      <style jsx global>
        {`
        html,
        body {
          background: #3C3C3C;
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
            Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
          overflow: none;
        }
        * {
          box-sizing: border-box;
        }
      `}
      </style>
    </div>
  );
};
export default Home;
