import FastVector from "fast-vector";
import Entity from "../Entity";
import Game from "../Game";

const Friction = {
  entityFilter(entity: Entity): boolean {
    return entity.components.velocity !== undefined && entity.components.mass !== undefined;
  },
  entities: new Set<Entity>(),
  update(tFrame: number, game: Game) {
    for (const entity of this.entities as Set<Entity>) {
      const {velocity} = entity.components;
      velocity.forces.push(velocity.normalize().mul(new FastVector(-.01, -.01)));
    }
  }
}

export default Friction;