import Entity from "../Entity";
import Game from "../Game";
export default function Bounce() {
  // rotate a vector
  function rotate([x, y], theta) {
    return [x * Math.cos(theta) - y * Math.sin(theta), x * Math.sin(theta) + y * Math.cos(theta)];
  }

  function collide(e1: Entity, e2: Entity) {
    const { position: p1, velocity: v1, circle: c1 } = e1.components;
    const { position: p2, velocity: v2, circle: c2 } = e2.components;
    const combinedRadius = c1.r + c2.r;
    if (p1.distanceTo(p2) < combinedRadius) {
      const [dvx, dvy] = [v1.x - v2.x, v1.y - v2.y];
      // if vectors intersect
      if (dvx * (p2.x - p1.x) + dvy * (p2.y - p1.y) >= 0) {
        // update velocities
        const angle = -Math.atan2(p2.y - p1.y, p2.x - p1.x);
        const [vx1, vy1] = rotate([v1.x, v1.y], angle);
        const [vx2, vy2] = rotate([v2.x, v2.y], angle);
        const [ux1, uy1] = rotate([vx1 * (c1.r - c2.r) / combinedRadius + vx2 * 2 * c2.r / combinedRadius, vy1], -angle);
        const [ux2, uy2] = rotate([vx2 * (c2.r - c1.r) / combinedRadius + vx1 * 2 * c1.r / combinedRadius, vy2], -angle);
        v1.x = ux1;
        v1.y = uy1;
        v2.x = ux2;
        v2.y = uy2;

        // Bounce
        p1.x += v1.x;
        p1.y += v1.y;
        p2.x += v2.x;
        p2.y += v2.y;

        // Recolor
        c1.hue = c1.hue + 5;
        c2.hue = c2.hue + 5;
      }
    }
  }
  const spatialHash: Set<Entity>[] = new Array();
  const collisions = new Map<Entity, Set<Entity>>();

  function collectCollision(e1: Entity, e2: Entity) {
    const { position: p1, circle: c1 } = e1.components;
    const { position: p2, circle: c2 } = e2.components;
    if (p1.distanceTo(p2) < c1.r + c2.r) {
      collisions.has(e1) ?
        collisions.get(e1).add(e2) :
        collisions.has(e2) ?
          collisions.get(e2).add(e1) :
          collisions.set(e1, new Set([e2]));
    }
  }

  const HASH_SIZE = 16;
  const NUMBER_OF_CELLS = Math.sqrt(HASH_SIZE);

  return {
    entityFilter(entity: Entity) {
      return entity.components.circle !== undefined;
    },
    entities: new Set<Entity>(),
    update(tFrame: number, game: Game) {
      const cellSize = [game.width / NUMBER_OF_CELLS, game.width / NUMBER_OF_CELLS];
      collisions.clear();
      spatialHash.splice(0, spatialHash.length);
      for (const entity of this.entities) {
        const { position, velocity, circle } = entity.components;
        if (!position || !velocity || !circle) continue;

        const topLeft = {
          x: Math.floor((position.x - circle.r) / cellSize[0]), 
          y: Math.floor((position.y - circle.r) / cellSize[1]),
        };
        const bottomRight = {
          x: Math.floor((position.x + circle.r) / cellSize[0]), 
          y: Math.floor((position.y + circle.r) / cellSize[1]),
        };

        for (let x = topLeft.x; x <= bottomRight.x; x++) {
          for (let y = topLeft.y; y <= bottomRight.y; y++) {
            const hashId = x + y * NUMBER_OF_CELLS;
            spatialHash[hashId]?.add(entity) ?? (spatialHash[hashId] = new Set([entity]));
          }
        }       

        if (position.x + circle.r >= game.width) {
          velocity.x *= -1;
          position.x = game.width - circle.r;
        }
        if (position.y + circle.r >= game.height) {
          velocity.y *= -1;
          position.y = game.height - circle.r;
        }
        if (position.x - circle.r <= 0) {
          velocity.x *= -1;
        }
        if (position.y - circle.r <= 0) {
          velocity.y *= -1;
        }
      }
      // compute collisions from spatialHash
      for (const cell of spatialHash) {
        if (cell?.size >= 1 !== true) continue;
        const array = Array.from(cell);
        array.forEach((entity, i) => {
          for (const neighbor of array.slice(i + 1)) {
            collectCollision(entity, neighbor);
          }
        });
      }

      for (const [e1, others] of collisions.entries()) {
        for (const e2 of others) {
          collide(e1, e2);
        }
      }
    }
  }
}
