import FastVector from "fast-vector";
import Entity from "../Entity";
import Game from "../Game";

const Velocity = {
  entityFilter(entity: Entity): boolean {
    return entity.components.velocity !== undefined && entity.components.position !== undefined;
  },
  entities: new Set<Entity>(),
  update(tFrame: number, game: Game) {
    for (const entity of this.entities as Set<Entity>) {
      const { position, velocity, mass } = entity.components;
      const netForce = velocity.forces.reduce((netForce, force) => {
        return netForce.add(force)
      }, FastVector.zero);
      if (mass) {
        velocity.accelleration = velocity.accelleration.add(
          FastVector.fromArray(netForce.toArray().map(f => f / mass.value) as [number, number])
        );
      } else {
        velocity.accelleration = velocity.accelleration.add(netForce);
      }
      velocity.forces.splice(0, velocity.forces.length);
      let {x,y} = velocity.add(velocity.accelleration)
      if (velocity.limit) {
        const lenSq = x ** 2 + y ** 2;
        if (lenSq > velocity.limit ** 2 && lenSq > 0) {
          const ratio = velocity.limit / Math.sqrt(lenSq);
          x *= ratio;
          y *= ratio;
        }
      }
      velocity.x = x;
      velocity.y = y;
      position.x += velocity.x * tFrame;
      position.y += velocity.y * tFrame;
    }
  }
}

export default Velocity;