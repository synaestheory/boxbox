import Entity from "../Entity";
import Game from "../Game";

let logged = false;

const imageCache = new Map<string, HTMLImageElement>();
function getImage(url) {
  if (imageCache.has(url)) {
    return imageCache.get(url);
  }
  const img = new Image();
  img.src = url;
  imageCache.set(url, img);
  return img;
}
export default {
  entityFilter(entity: Entity) {
    const { position, circle, words, rectangle, assets, glow } = entity.components;
    return position !== undefined && (circle || words || rectangle || assets) !== undefined
  },
  entities: new Set<Entity>(),
  update(tFrame: number, game: Game) {
    const context = game.context;
    context.clearRect(0, 0, context.canvas.width, context.canvas.height);
    for (const entity of this.entities) {
      const position = entity.components.position;
      if (typeof entity.components.circle !== "undefined") {
        const circle = entity.components.circle;
        context.beginPath();
        context.arc(
          position.x,
          position.y,
          circle.r,
          0,
          2 * Math.PI
        );
        if (circle.fill) {
          context.fillStyle = circle.fill;
          context.fill();
        }
      }

      if (typeof entity.components.words !== "undefined") {
        const maxWidth = game.width * 0.9;
        const words = entity.components.words;
        
        if (words.hAlign) {
          position.x = game.width * words.hAlign;
        }

        if (words.vAlign) {
          position.y = game.height * words.vAlign;
        }

        context.font = `${words.size}px sans-serif`
        context.fillStyle = words.color;
        context.textAlign = words.align as CanvasTextAlign;
        if (context.measureText(words.text).width < maxWidth) {
          context.fillText(words.text, position.x, position.y);
        } else {
          let lineNumber = 0;
          const lastLine = words.text.split(' ').reduce((line, word) => {
            const next = `${line} ${word}`;
            if (context.measureText(next).width < maxWidth){
              return next;
            } else {
              context.fillText(line, position.x, position.y + (words.size * lineNumber));
              lineNumber += 1;
              return word;
            }
          }, "");
          context.fillText(lastLine, position.x, position.y + (words.size * lineNumber));
        }
      }

      if (typeof entity.components.assets !== "undefined") {
        const assets = entity.components.assets;
        assets.image.forEach(image => {
          context.drawImage(getImage(image.url), position.x + image.x, position.y + image.y, image.width, image.height);
        });
      }

      if (typeof entity.components.glow !== "undefined") {
        const glow = entity.components.glow;
        context.shadowColor = glow.color;
        context.shadowBlur = glow.blur;
        context.fill();
        context.shadowColor = "";
        context.shadowBlur = 0;
      }
    }
  }
}