import Entity from "../Entity";
import Game from "../Game";

type SystemUpdate = (tFrame: number, game: Game) => void;

export default interface System {
  entityFilter: (entity: Entity) => boolean;
  entities: Set<Entity>;
  update: SystemUpdate;
}

