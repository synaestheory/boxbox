import Entity from "../Entity";
import Game from "../Game";

interface Click {
  x: number;
  y: number;
}

export default {
  addClick(event: MouseEvent | TouchEvent) {
    this.clicks.push(this.getRelativeClick(event))
  },
  game: null,
  clicks: [] as Click[],
  entityFilter(entity) {
    return entity.components.clickable !== undefined
  },
  entities: new Set<Entity>(),
  update(tFrame: number, game: Game) {
    if (!this.game) {
      game.canvas.addEventListener("click", (event) => this.addClick(event));
      game.canvas.addEventListener("touchstart", (event) => this.addClick(event));
      game.canvas.addEventListener("touchend", (event) => this.addClick(event));
      this.game = game;
    }
    let click: Click = this.clicks.pop();
    // cache this so new entities don't register clicks
    const ents: Entity[] = Array.from(this.entities);
    while (click) {
      for (const entity of ents) {
        entity.components.clickable.onClick(click, game);
      }
      click = this.clicks.pop();
    }
  },
  getRelativeClick(event): Click {
    const styles = getComputedStyle(this.game.canvas);
    const w = parseInt(styles.width);
    const h = parseInt(styles.height);
    return {
      x: (event.pageX - this.game.canvas.offsetLeft) * this.game.width / w,
      y: (event.pageY - this.game.canvas.offsetTop) * this.game.height / h,
    }
  }
}