import Entity from "../Entity";
import Game from "../Game";

export default {
  entityFilter(entity: Entity) {
    const { position, circle } = entity.components;
    return position !== undefined && circle !== undefined;
  },
  entities: new Set<Entity>(),
  update(tFrame: number, game: Game) {
    for (const entity of this.entities) {
      const { position, circle } = entity.components;
      if (circle) {
        position.x = Math.max(0 + circle.r, Math.min(game.width - circle.r, position.x));
        position.y = Math.max(0 + circle.r, Math.min(game.height - circle.r, position.y));
      }
    }
  }
}
