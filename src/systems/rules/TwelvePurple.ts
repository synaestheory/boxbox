import Circle from "../../components/Circle";
import Glow from "../../components/Glow";
import HueTween from "../../components/HueTween";
import Ball from "../../entities/Ball";
import Entity from "../../Entity";
import Game from "../../Game";

export default function TwelvePurple(OnWin: Function) {
  let time = 0;
  let winAlerted = false;
  return {
    start(game: Game) {
      time = 0;
      Array(1).fill(0).forEach(() => {
        const huetween = new HueTween(0);
        const circle = new Circle(Math.min(game.width, game.height) * .9, 235);
        game.addEntity(new Ball({huetween, circle}, game));
      });
    },
    userMessage: "Make 12 purple balls as quickly as possible.",
    entityFilter(entity: Entity) {
      return entity.components.circle !== undefined;
    },
    entities: new Set<Entity>(),
    update(tFrame: number, game: Game) {
      time += tFrame;
      let purpleCount = 0;
      for (const entity of this.entities) {
        const { circle, glow } = entity.components;
        if (circle.hue >= 255 && circle.hue <= 285) {
          entity.addComponent(new Glow("white", 20));
          purpleCount++;
        } else if (glow) {
          entity.removeComponent(glow);
        }
      }
      if (purpleCount >= 12 && !winAlerted) {
        const seconds = time / 1000;
        OnWin(`${seconds.toFixed(2)} seconds!`);
      }
    }
  }
}
