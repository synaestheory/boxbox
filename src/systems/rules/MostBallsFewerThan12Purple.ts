import Circle from "../../components/Circle";
import Glow from "../../components/Glow";
import HueTween from "../../components/HueTween";
import Ball from "../../entities/Ball";
import Entity from "../../Entity";
import Game from "../../Game";

export default function MostBallsFewerThan12Purple(OnWin: Function) {
  let time = 0;
  let winAlerted = false;
  return {
    start(game: Game) {
      time = 0;
      Array(1).fill(0).forEach(() => {
        const huetween = new HueTween(0);
        const circle = new Circle(Math.min(game.width, game.height) * .9, 235);
        game.addEntity(new Ball({huetween, circle}, game));
      });
    },
    userMessage: "Make the most balls while having fewer than 12 purple balls.",
    entityFilter(entity: Entity) {
      return entity.components.circle !== undefined;
    },
    entities: new Set<Entity>(),
    update(tFrame: number, game: Game) {
      time += tFrame;
      let purpleCount = 0;
      for (const entity of this.entities) {
        const { circle, glow } = entity.components;
        if (circle.hue >= 255 && circle.hue <= 285) {
          entity.addComponent(new Glow("red", 20));
          purpleCount++;
        } else if (glow) {
          entity.removeComponent(glow);
        }
      }
      if (purpleCount >= 12 && !winAlerted) {
        OnWin(`${this.entities.size} balls!`, this);
      }
    }
  }
}
