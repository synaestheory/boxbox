import System from "..";
import Game from "../../Game";

export default interface WinCondition extends System {
  userMessage: string;
  start(game: Game): void;  
}