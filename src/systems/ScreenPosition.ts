import Position from "../components/Position";
import Entity from "../Entity";
import Game from "../Game";

export default {
  entityFilter(entity: Entity) {
    const { screenposition } = entity.components;
    return screenposition !== undefined;
  },
  entities: new Set<Entity>(),
  update(tFrame: number, game: Game) {
    for (const entity of this.entities) {
      if (entity.components.position === undefined) {
        entity.addComponent(new Position(0, 0));
      }
      const {position, screenposition } = entity.components;
      if (screenposition.hAlign) {
        position.x = game.width * screenposition.hAlign + screenposition.hOffset;
      }
      if (screenposition.vAlign) {
        position.y = game.height * screenposition.vAlign + screenposition.vOffset;
      }
    }
  }
}
