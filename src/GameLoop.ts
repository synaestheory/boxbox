type UpdateListener = (tFrame: number) => void;
export default class GameLoop {
  private stopped: boolean;
  private stopHandle: number;
  private lastTimestamp: number;
  private updateListener: UpdateListener;
  constructor(updateListener: UpdateListener) {
    this.start();
    this.updateListener = updateListener;
    this.stop = this.stop.bind(this);
  }
  stop() {
    this.stopped = true;
    cancelAnimationFrame(this.stopHandle);
  }
  start() {
    this.stopped = false;
    this.main(0);
  }
  main(tFrame) {
    if (this.stopped) return;
    this.stopHandle = requestAnimationFrame(this.main.bind(this));
    if (!this.lastTimestamp) {
      this.lastTimestamp = tFrame;
    }
    this.update(tFrame - this.lastTimestamp);
    this.lastTimestamp = tFrame;
  }
  update(timeDelta) {
    if (this.updateListener) {
      this.updateListener(timeDelta);
    }
  }
}
