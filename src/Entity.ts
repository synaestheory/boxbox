import Components, { Component, ComponentType } from "./components";

const idGenerator = (function* idGenerator() {
  let count = 0;
  while (true) {
    yield `${Math.random().toString(36).substr(2)}-${count += 1}`;
  }
}());

export default class Entity {
  id: string;
  components: Components;
  constructor(...components: Component[]) {
    this.id = idGenerator.next().value as string;
    this.components = {};
    components.forEach(this.addComponent.bind(this))
  }
  addComponent(component: Component) {
    if (typeof component?.type !== "string") {
      console.warn("Missing component.type", component);
      return;
    }
    this.components[component.type] = component as any;
  }
  removeComponent(component: Component | ComponentType) {
    if (typeof component === "string") {
      delete this.components[component];
    } else {
      delete this.components[component.type];
    }
  }
}
