export default class Clickable {
  type = "clickable" as const;
  onClick: Function;
  constructor(onClick: Function) {
    this.onClick = onClick;
  }
}