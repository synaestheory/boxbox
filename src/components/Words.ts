export default class Words {
  type = "words" as const;
  text: string;
  size: number;
  color: string;
  align: CanvasTextAlign;
  constructor({ text, size = 48, color, align }: Partial<Words>) {
    this.align = align;
    this.text = text;
    this.size = size;
    this.color = color;
  }
}