export interface ImageAsset {
  url: string;
  x: number,
  y: number,
  width: number,
  height: number,
}

export default class Assets {
  type = "assets" as const;
  image: ImageAsset[];
  constructor({image}) {
    this.image = image;
  }
}