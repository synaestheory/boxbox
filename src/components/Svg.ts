export interface SvgPart {
  fillStyle?: string;
  fill?: string;
  stroke?: string;
  strokeStyle?: string;
}

export default class Svg {
  type = "svg" as const;
  parts: SvgPart[]
  constructor({parts}: Omit<Svg, "type">) {
    this.parts = parts;
  }
}