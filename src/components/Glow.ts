export default class Glow {
  type = "glow" as const;
  color: string;
  blur: number;
  constructor(color: string, blur: number) {
    this.color = color;
    this.blur = blur;
  }
}