export default class ScreenPosition {
  type = "screenposition" as const;
  hAlign: number;
  hOffset: number;
  vAlign: number;
  vOffset: number;
  constructor(hAlign: number, vAlign: number, hOffset = 0, vOffset = 0) {
    this.hAlign = hAlign;
    this.vAlign = vAlign;
    this.hOffset = hOffset;
    this.vOffset = vOffset;
  }
}