export default class HueTween {
  type = "huetween" as const;
  factor: number;
  constructor(factor: number) {
    this.factor = factor;
  }
}