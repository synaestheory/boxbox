export default class Rectangle {
  type = "rectangle" as const;
  width: number;
  height: number;
  fill?: string;
  stroke?: string;
  constructor({ width, height, fill, stroke }: Omit<Rectangle, "type">) {
    this.width = width;
    this.height = height;
    this.fill = fill;
    this.stroke = stroke;
  }
}