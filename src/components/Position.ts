export interface Coordinates {
  x: number;
  y: number;
}

export default class Position implements Coordinates {
  type = "position" as const;
  x: number;
  y: number;
  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
  flip() {
    const x = this.y;
    const y = this.x;
    this.x = x;
    this.y = y;
  }
  distanceTo(there: Coordinates) {
    return Math.sqrt((there.x - this.x) ** 2 + (there.y - this.y) ** 2);
  }
}