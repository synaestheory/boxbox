import FastVector from 'fast-vector';

export default class Velocity extends FastVector {
  type = "velocity" as const;
  forces: FastVector[];
  accelleration: FastVector;
  limit: number;
  constructor(x: number, y: number, limit?: number) {
    super(x, y);
    this.limit = limit;
    this.forces = [];
    this.accelleration = FastVector.zero;
  }
}