export default class Circle {
  type = "circle" as const;
  private _hue: number;
  size: number;
  r: number;
  fill: string;
  constructor(size: number, hue?: number) {
    this.size = size;
    this.r = size / 2;
    this.hue = hue ?? Math.floor(Math.random() * 360);
  }
  set hue(hue: number) {
    this._hue = hue % 360;
    this.fill = `hsl(${this._hue}, 50%, 50%)`;
  }
  get hue() {
    return this._hue;
  }
}