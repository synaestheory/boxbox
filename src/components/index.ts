import Assets from "./Assets";
import Circle from "./Circle";
import Clickable from "./Clickable";
import ConstrainPosition from "./ConstrainPosition";
import Glow from "./Glow";
import HueTween from "./HueTween";
import Mass from "./Mass";
import Position from "./Position";
import Rectangle from "./Rectangle";
import ScreenPosition from "./ScreenPosition";
import Svg from "./Svg";
import Velocity from "./Velocity";
import Words from "./Words";

export type Component =
  | Assets
  | Circle
  | Clickable
  | ConstrainPosition
  | Glow
  | HueTween
  | Mass
  | Position
  | Rectangle
  | ScreenPosition
  | Svg
  | Velocity
  | Words;

export type ComponentType = Component["type"];

type ComponentMap<U> = {
  [K in ComponentType]: U extends { type: K } ? U : never;
};
type AllComponents = ComponentMap<Component>;
type Components = Partial<AllComponents>;
export default Components;