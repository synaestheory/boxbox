export default class Mass {
  type = "mass" as const;
  value: number;
  constructor(mass: number) {
    this.value = mass;
  }
}
