import Entity from "../Entity";
import Position from '../components/Position';
import Clickable from "../components/Clickable";
import Assets from "../components/Assets";
import { Component } from "../components";
import ScreenPosition from "../components/ScreenPosition";

interface ButtonProps {
  size: number;
  onClick: Function;
  image: string;
  hAlign: number;
  vAlign: number;
}
export default class Button extends Entity {
  private _onClick: Function;
  private size: number;
  constructor(...components: Component[]) {
    super(...components);
    const assets = this.components.assets;
    this.size = assets.image[0].width;
  }
  static fromProps({size, onClick, image, hAlign, vAlign}: ButtonProps): Button {
    const button = new Button(
      new Position(0, 0),
      new Assets({image: [{url: image, x: 0, y: 0, width: size, height: size}]}),
      new ScreenPosition(hAlign, vAlign, size / -2, size / -2),
    );
    button.setOnClick(onClick);
    return button;
  }
  onClick(click, game) {
    if (this.components.position.distanceTo(click) < this.size) {
      this._onClick(click, game);
    }
  }
  setOnClick(onClick) {
    this.addComponent(new Clickable(this.onClick.bind(this)))
    this._onClick = onClick;
  }
}
