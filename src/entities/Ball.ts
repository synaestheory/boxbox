import Entity from "../Entity";
import Circle from '../components/Circle';
import Clickable from '../components/Clickable';
import Position from '../components/Position';
import ConstrainPosition from "../components/ConstrainPosition";
import Velocity from "../components/Velocity";
import Components from "../components";
import HueTween from "../components/HueTween";
import Game from "../Game";
import Mass from "../components/Mass";

export default class Ball extends Entity {
  constructor(components: Components, game: Game) {
    const { position, velocity, huetween } = components ?? {};
    const circle = components.circle ?? new Circle(Math.min(game.width, game.height) * .2);
    const MAX_X = game.width - circle.size;
    const MAX_Y = game.height - circle.size;
    super(
      circle,
      new Mass(circle.r),
      position ?? new Position(
        Math.round(circle.r + Math.random() * MAX_X),
        Math.round(circle.r + Math.random() * MAX_Y),
      ),
      huetween ?? new HueTween(0),
      new Clickable((click, game) => {
        if (this.isClicked(click)) {
        const { circle, position, huetween } = this.components;
          let count = 4;
          while (count) {
            // 4 corners
            const xF = count > 2 ? ((-1) ** count) : -((-1) ** count);
            const yF = count < 2 && count != 1 ? ((-1) ** count) : -((-1) ** count);
            const x = position.x + circle.r * xF * .5;
            const y = position.y + circle.r * yF * .5;
            // diagonal angles
            const angle = (-45 + 90 * (count)) * (Math.PI / 180);
            const components = {
              circle: new Circle(circle.r, circle.hue + huetween.factor * count),
              huetween: new HueTween(huetween.factor),
              position: new Position(x, y),
              velocity: new Velocity(xF * Math.random() * .2, yF * Math.random() * .2),
            };
            game.addEntity(new Ball(components, game));
            count--;
          }
          game.deleteEntity(this);
        }
      }),
      new ConstrainPosition(),
      velocity ?? new Velocity(Math.random() * .2, Math.random() * .2),
    );
  }
  isClicked(click): boolean {
    const { position, circle } = this.components;
    return position.distanceTo(click) < circle.r;
  }
}
