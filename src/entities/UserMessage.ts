import Entity from "../Entity";
import Position from '../components/Position';
import Words from '../components/Words';
import ScreenPosition from "../components/ScreenPosition";

export default class UserMessage extends Entity {
  static fromProps({x, y, text, size, color, hAlign, vAlign}) {
    return new UserMessage(
      new Position(x, y),
      new Words({
        text,
        size,
        align: "center" as CanvasTextAlign,
        color,
      }),
      new ScreenPosition(hAlign, vAlign),
    )
  }
}
