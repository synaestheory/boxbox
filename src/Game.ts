import GameLoop from "./GameLoop";
import Ball from "./entities/Ball";
import RenderingSystem from "./systems/rendering";
import Entity from "./Entity";
import MouseSystem from "./systems/MouseTouch";
import ConstrainPositionSystem from "./systems/ConstrainPosition";
import VelocitySystem from "./systems/Velocity";
import BounceSystem from "./systems/Bounce";
import TwelvePurple from "./systems/rules/TwelvePurple";
import UserMessage from "./entities/UserMessage";
import Button from "./entities/Button";
import System from "./systems";
import Rules from "./systems/rules";
import TwelvePurple2 from "./systems/rules/TwelvePurple2";
import MostBallsFewerThan12Purple from "./systems/rules/MostBallsFewerThan12Purple";
import Gravity from "./systems/Gravity";
import Friction from "./systems/Friction";
import ScreenPosition from "./systems/ScreenPosition";

const BUTTON_SIZE = 80;
export default class Game {
	private loop: GameLoop;
	private _canvas?: HTMLCanvasElement;
	private rules: ((OnWin: Function) => Rules)[];
	width: number;
	height: number;
	private systems: System[];
	private entities: Set<Entity>;
	context: CanvasRenderingContext2D;
	constructor(width, height) {
		this.entities = new Set();
		this.loop = new GameLoop(this.update.bind(this));
		this.width = width;
		this.height = height;
		this.systems = [];
	}
	update(tFrame: number) {
		for (const system of this.systems) {
			system.update(tFrame, this);
		}
	}
	resize(width, height) {
		this.canvas.width = width;
		this.canvas.height = height;
		const clientRect = this.canvas.getBoundingClientRect();
		const dpr = window.devicePixelRatio ?? 1;
		this.canvas.width = clientRect.width * dpr;
		this.canvas.height = clientRect.height * dpr;
		this.context = this.canvas.getContext("2d");
		this.context.scale(dpr, dpr);
		for (const entity of this.entities) {
			const { position, circle } = entity.components;
			if (circle) {
				const max_x = this.canvas.width - circle.r;
				const max_y = this.canvas.height - circle.r;
				if (position.x > max_x) {
					position.x = Math.max(
						0 + circle.r,
						Math.min(max_x, Math.random() * max_x)
					);
				}
				if (position.y > max_y) {
					position.y = Math.max(
						0 + circle.r,
						Math.min(max_y, Math.random() * max_y)
					);
				}
			}
		}
	}
	orientationChange() {
		const { width, height } = this;
		this.width = height;
		this.height = width;
		this.entities.forEach(({ components: { position } }) => {
			if (position) position.flip();
		});
	}
	onWin(winningMessage) {
		this.systems.pop();
		this.clearEntities();
		this.addEntity(
			UserMessage.fromProps({
				x: this.width / 2,
				y: this.height / 2,
				hAlign: 0.5,
				vAlign: 0.5,
				text: winningMessage,
				size: 24,
				color: "white",
			})
		);
		this.addEntity(
			Button.fromProps({
				hAlign: 0.5,
				vAlign: 0.75,
				size: BUTTON_SIZE,
				image: "PlayArrow.png",
				onClick: () => this.setWinConditions(),
			})
		);
	}
	set canvas(canvas: HTMLCanvasElement) {
		this._canvas = canvas;
		this._canvas.width = this.width;
		this._canvas.height = this.height;
		const clientRect = this.canvas.getBoundingClientRect();
		const dpr = window.devicePixelRatio ?? 1;
		this._canvas.width = clientRect.width * dpr;
		this._canvas.height = clientRect.height * dpr;
		this.context = this._canvas.getContext("2d");
		this.context.scale(dpr, dpr);
		this.init();
	}
	get canvas() {
		return this._canvas;
	}

	addEntity = (entity: Entity) => {
		this.entities.add(entity);
		this.systems.filter((system) => {
			if (system.entityFilter(entity)) {
				system.entities.add(entity);
			}
		});
	};
	deleteEntity(entity: Entity) {
		this.entities.delete(entity);
		this.systems.forEach((system) => {
			system.entities.delete(entity);
		});
	}
	clearEntities() {
		this.entities.clear();
		this.systems.forEach((system) => {
			system.entities.clear();
		});
	}
	init() {
		this.systems = [
			MouseSystem,
			ScreenPosition,
			BounceSystem(),
			VelocitySystem,
			ConstrainPositionSystem,
			RenderingSystem,
		];
		this.rules = [TwelvePurple, TwelvePurple2, MostBallsFewerThan12Purple];
		this.setWinConditions();
	}
	setWinConditions() {
		const createConditions = this.rules.shift();
		this.rules.push(createConditions);
		const conditions = createConditions(this.onWin.bind(this));
		this.systems.push(conditions);
		this.showInstructions(conditions);
	}
	showInstructions(conditions) {
		this.clearEntities();
		this.addEntity(
			UserMessage.fromProps({
				x: this.width * 0.5,
				y: this.height * 0.25,
				hAlign: 0.5,
				vAlign: 0.25,
				text: conditions.userMessage,
				color: "White",
				size: 48,
			})
		);
		this.addEntity(
			Button.fromProps({
				hAlign: 0.5,
				vAlign: 0.75,
				size: BUTTON_SIZE,
				onClick: () => this.startGame(conditions),
				image: "PlayArrow.png",
			})
		);
	}
	startGame(conditions: Rules) {
		this.clearEntities();
		conditions.start(this);
	}
}
